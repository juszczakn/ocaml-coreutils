(* consts *)
let program_name = "pwd"
let usage_info = "No options supported yet (-P, -L)"

let opts = Coreutils.Cli.default_cli_opts program_name

(* fn *)
let pwd () = print_endline @@ Sys.getcwd ()

(* main *)
let main () =
  if (Coreutils.Cli.no_params ()) then
    (pwd ())
  else
    Arg.parse opts (fun (_) -> (pwd ())) usage_info

let () = main ()
