let program_name = "yes"
let usage_info = "yes [OPTIONS]
                  yes [STRING]
                  Repeatedly output a line with all specified STRING(s), or 'y'.
                  "

let opts = Coreutils.Cli.default_cli_opts program_name

let yes s =
  while true do
    Printf.printf "%s\n" s
  done

let main () =
  if (Coreutils.Cli.no_params ()) then
    (yes "y")
  else
    Arg.parse opts yes usage_info

let () = main ()
