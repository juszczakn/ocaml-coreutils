open Core

let summary = "Print the user name associated with the current effective user ID."
let usage_info = "whoami [OPTIONS]"

let get_username () = ()
                      |> User_and_group.for_this_process_exn
                      |> User_and_group.user

let print_username () = Printf.printf "%s\n" (get_username ())

let spec = Command.Spec.empty

let command = Command.basic_spec
                ~summary:summary
                ~readme:(fun () -> usage_info)
                spec
                (fun () -> print_username ())

let () = Command.run
           ~version:Coreutils.Cli.current_version
           command
