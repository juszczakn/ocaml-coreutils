open Core

let summary = "strip directory and suffix from filenames"
let readme = ""

let remove_suffix suffix path =
  let sfx = String.of_string suffix in
  match String.chop_suffix path ~suffix:sfx with
  |None -> path
  |Some p -> String.to_string p

let print_basename ?suffix multiple zero paths =
  let paths = if multiple then paths else (List.take paths 1) in
  List.iter paths
    ~f:(fun p -> let path = match suffix with
                   | None -> p
                   | Some sfx -> (remove_suffix sfx p)
                 in
                 let basename = (Filename.basename path) in
                 if zero then
                   (print_string basename)
                 else
                   (print_endline basename))

let spec =
  let open Command.Spec in
  empty
  +> flag "-s" ~aliases:["-suffix"] (optional string) ~doc:"suffix the suffix to remove"
  +> flag "-a" ~aliases:["-multiple"] no_arg
       ~doc:"multiple support multiple arguments and treat each as a NAME"
  +> flag "-z" ~aliases:["-zero"] no_arg ~doc:"zero end each output line with NUL, not newline"
  +> anon (sequence ("paths" %: string))

let command = Command.basic_spec
                ~summary:summary
                ~readme:(fun () -> readme)
                spec
                (fun suffix multiple zero paths () -> print_basename ?suffix multiple zero paths)

let () = Command.run
           ~version:Coreutils.Cli.current_version
           command
