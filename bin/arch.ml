open Core

let summary = "print machine hardware name (same as uname -m)"
let readme = "arch [OPTIONS]"

let arch () = Unix.Utsname.machine (Unix.uname ())
let print_arch () = print_endline (arch ())

let spec = Command.Spec.empty

let command = Command.basic_spec
                ~summary:summary
                ~readme:(fun () -> readme)
                spec
                (fun () -> print_arch ())


let () = Command.run
           ~version:Coreutils.Cli.current_version
           command
