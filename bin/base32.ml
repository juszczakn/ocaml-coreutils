open Core

let summary = "base32"
let readme = ""

let command = Command.basic
                ~summary:summary
                ~readme:(fun () -> readme)
                (let open Command.Let_syntax in
                 [%map_open
                  let file = anon ("FILE" %: file)
                      in
                      fun () -> (Coreutils.Base32_lib.print_file_base32 file)
                ])

let () = Command.run
           ~version:Coreutils.Cli.current_version
           command
