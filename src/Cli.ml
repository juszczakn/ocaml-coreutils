let current_version = "0.1.0"

let version_info program_name =
  fun () -> Printf.printf "%s (OCaml CoreUtils) version %s\n"
              program_name
              current_version

let default_cli_opts program_name = [
    ("--version", Arg.Unit (version_info program_name), "display current version");
  ]

let no_params () = (Array.length Sys.argv) = 1

let print_unknown_param program_name unknown_op =
  let () = Printf.printf
             "%s: extra operand '%s'\nTry '%s' --help for more info.\n"
             program_name
             unknown_op
             program_name in
  exit 1
