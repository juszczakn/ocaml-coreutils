val current_version : string
(** Top-level version of OCaml-Coreutils *)

val default_cli_opts : string -> (Arg.key * Arg.spec * Arg.doc) list
(** Default cli opts: version, help flag *)

val no_params : unit -> bool
(** Whether any params were passed in *)

val print_unknown_param : string -> string -> unit
(** Print error string if unknown op was passed in *)
