open Core

(* tmp for now, just need a way to create ==== padding *)
let rec repeated_str n s =
  if n = 0 then "" else s ^ repeated_str (n - 1) s

let int_to_b32_str =
  let mapping_alist = [
      0,"A"; 1,"B"; 2,"C"; 3,"D"; 4,"E"; 5,"F"; 6,"G"; 7,"H"; 8,"I"; 9,"J";
      10,"K"; 11,"L"; 12,"M"; 13,"N"; 14,"O"; 15,"P"; 16,"Q"; 17,"R"; 18,"S";
      19,"T"; 20,"U"; 21,"V"; 22,"W"; 23,"X"; 24,"Y"; 25,"Z"; 26,"2"; 27,"3";
      28,"4"; 29,"5"; 30,"6"; 31,"7"
    ]
  in
  Map.of_alist_exn (module Int) mapping_alist

let print_file_base32 filepath =
  let jagged_bits = Bitstring.bitstring_of_file filepath
  in
  let jagged_bits_len = Bitstring.bitstring_length jagged_bits
  in
  let padding = 40 - (jagged_bits_len mod 40)
  in
  let bits = Bitstring.concat [jagged_bits; (Bitstring.zeroes_bitstring padding)]
  in
  (* let open Bitstring
   * in *)
  (* This looks like this should be tail recursive,
       but I dont think it is right now *)
  let rec convert_bits bitstr result =
    (* note that it looks like a match, but it's not exhaustive,
         so have to handle that manually *)
    match%bitstring bitstr with
    (* the next 40 bits, 8 groups of 5-bits each (a-h) *)
    |{|
      a : 5; b : 5; c : 5; d : 5;
      e : 5; f : 5; g : 5; h : 5
      |} ->
      begin
        let curr_result = (List.map [a; b; c; d; e; f; g; h] ~f:(fun i -> (Map.find_exn int_to_b32_str i)))
                          |> String.concat
                          |> (fun part -> String.concat [result; part])
        in
        if Bitstring.bitstring_length bitstr = 40 then
          (* we're done here *)
          curr_result
        else
          (convert_bits (Bitstring.dropbits 40 bitstr) curr_result)
      end
  in
  let b32 = convert_bits bits ""
  in
  (Printf.printf "%s%s\n"
     (String.drop_suffix b32 (padding / 5))
     (repeated_str (padding / 5) "="))
