# ocaml-coreutils

https://gitlab.com/juszczakn/ocaml-coreutils

## Project Goals

This is simply a project I'm using for learning OCaml. Ideally it will closely mirror the
options/flags of GNU Coreutils, but not necessarily. For example, utils may use single '-'
dashes for options, etc.

## Building

Build tool is jbuilder.

Install dependencies: `opam install jbuilder core bitstring`

Build an individual binary:
```
# Build an individual binary under bin/ directory
jbuilder build bin/yes.exe

# run the created binary
./_build/default/bin/yes.exe
```

## Utilities Started

- true
- false
- pwd
- yes
- whoami
- arch
- basename
- base32
