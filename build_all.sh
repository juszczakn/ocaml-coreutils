#!/bin/sh

set -e

for FILE in bin/*.ml
do
    dune build ${FILE%.ml}.exe
done
